﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicVideoCatalog.DataTypes
{
    public class MissingEntry
    {
        public string ArtistName;
        public string EntryName;
    }
}
