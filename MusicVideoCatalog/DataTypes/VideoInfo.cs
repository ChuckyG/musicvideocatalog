﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace MusicVideoCatalog.DataTypes
{
    public class VideoInfo
    {
        public int height;
        public int width;
        public long size;
        public double duration;
        public string VideoType;

        public static IEnumerable<(string, int, string)> VideoRowComments(List<CollectionEntry> entries, string ColumnName)
        {
            var rowComments = new List<(string, int, string)>();
            foreach (var commentObject in entries.Select((x, i) => new { Object = x, Index = i }))
            {
                var comment = commentObject.Object.VideoInfo.GetAllVideoDescriptions(true);
                if (comment != null)
                    rowComments.Add((ColumnName, commentObject.Index, comment));
            }
            return rowComments;
        }
    }

    public static class VideoInfoExtensions
    {
        public static string GetComment(this ConcurrentBag<VideoInfo> videoInfoBag)
        {
            if (videoInfoBag == null || videoInfoBag.Count < 1)
                return string.Empty;
                        
            var videoInfoList = videoInfoBag.OrderBy(y => y.VideoType, StringComparer.OrdinalIgnoreCase).ToList();
            if (videoInfoList.Count() > 1)
            {
                return $"{videoInfoList.Count()} Videos";
            }

            var videoInfo = videoInfoList.First();
            return GetVideoDescription(videoInfo);            
        }

        public static string GetAllVideoDescriptions(this ConcurrentBag<VideoInfo> videoInfoBag, bool moreThanOne=false)
        {
            var entryCount = videoInfoBag.Count();
            if (entryCount < 1 || (moreThanOne && entryCount < 2))
                return null;

            var result = "";
            foreach (var videoInfo in videoInfoBag)
            {
                result += GetVideoDescription(videoInfo);
                result += $"\r\n";
            }

            return result[0..^2];
        }

        public static string GetVideoDescription(VideoInfo videoInfo)
        {
            var result = $"{videoInfo.VideoType}: ";
            if (videoInfo.width > 0 && videoInfo.height > 0)
                result += $"{videoInfo.width} x {videoInfo.height}";
            if (videoInfo.duration > 0)
            {
                var elapsed = TimeSpan.FromSeconds(videoInfo.duration);
                result += $", {elapsed:h\\:mm\\:ss}";
            }
            return result;
        }

        public static bool HasMethod(this object objectToCheck, string methodName)
        {
            var type = objectToCheck.GetType();
            return type.GetMethod(methodName) != null;
        }
    }
}
