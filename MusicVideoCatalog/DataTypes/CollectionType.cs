﻿namespace MusicVideoCatalog.DataTypes
{

    // three types of collections
    // 1 - foldernames under an artist name
    // 2 - foldernames under a root
    // 3 - individual tracks in a folder
    public enum CollectionType
    {
        Unknown = -1,
        Undefined = 0,
        ArtistFolders = 1,
        RootFolders,
        IndividualTracks
    }

}
