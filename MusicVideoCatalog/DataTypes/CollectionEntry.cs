﻿using System.Collections.Concurrent;

namespace MusicVideoCatalog.DataTypes
{
    public class CollectionEntry
    {
        public string EntryName;
        public string ArtistName;
        public long EntrySize;
        public string RootFolder;
        public ConcurrentBag<VideoInfo> VideoInfo;
        public bool HasDVDs;
        public bool HasBDs;
        public bool HasVideoFiles;
        public bool IsBootleg;
    }
}

