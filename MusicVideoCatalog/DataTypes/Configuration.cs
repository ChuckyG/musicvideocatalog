﻿using Alturos.VideoInfo;
using System.Collections.Generic;

namespace MusicVideoCatalog.DataTypes
{
    public class Configuration
    {
        public string OutputPathAndFile;
        public IEnumerable<string> SkipExtensions;
        public HashSet<string> SkipExtensionsHash;
        public IEnumerable<string> VideoExtensions;
        public HashSet<string> VideoExtensionsHash;
        public string MissingExtension;
        public IEnumerable<string> DVDExtensions;
        public HashSet<string> DVDExtensionsHash;
        public IEnumerable<string> BDExtensions;
        public HashSet<string> BDExtensionsHash;
    }
}
