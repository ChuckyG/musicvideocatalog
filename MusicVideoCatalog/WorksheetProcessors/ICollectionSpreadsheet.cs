﻿using ClosedXML.Excel;
using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicVideoCatalog.WorksheetProcessors
{
    public interface ICollectionSpreadsheet
    {
        public void Header(IXLWorksheet ws, IXLRow headerRow);
        public IEnumerable<object> Row(List<CollectionEntry> entries);
        public IEnumerable<(string, int, string)> RowComments(List<CollectionEntry> entries);
        public string SortOrder();
    }
}
