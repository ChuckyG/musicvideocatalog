﻿using ClosedXML.Excel;
using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicVideoCatalog.WorksheetProcessors
{
    public class VideoSeries : ICollectionSpreadsheet
    {
        public void Header(IXLWorksheet ws, IXLRow headerRow)
        {
            headerRow.Style.Fill.BackgroundColor = XLColor.SteelBlue;
            headerRow.Style.Font.FontColor = XLColor.White;
            headerRow.Style.Font.Bold = true;
             
            string[] columns = { "FoundInFolder", "Title", "Size", "VideoTypes", "SizeInBytes" };
            for (var i = 0; i < columns.Length; i++)
            {
                headerRow.Cell(i + 1).Value = columns[i];
            }

            var byteSizeColumn = ws.Column(5);
            byteSizeColumn.Style.NumberFormat.NumberFormatId = 3;
        }

        public IEnumerable<object> Row(List<CollectionEntry> entries)
        {
            return entries.Select(m => new
            {
                m.RootFolder,
                Title = m.EntryName,
                Size = m.EntrySize.ToSize(2),
                VideoTypes = m.VideoInfo.GetComment(),
                SizeInBytes = m.EntrySize
            });
        }
        public IEnumerable<(string, int, string)> RowComments(List<CollectionEntry> entries)
        {
            return VideoInfo.VideoRowComments(entries, "D");
        }

        string ICollectionSpreadsheet.SortOrder()
        {
            return "FoundInFolder, Title, SizeInBytes ASC";
        }

    }
}
