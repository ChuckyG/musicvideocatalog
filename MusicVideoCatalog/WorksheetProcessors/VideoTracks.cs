﻿using ClosedXML.Excel;
using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MusicVideoCatalog.WorksheetProcessors
{
    public class VideoTracks : ICollectionSpreadsheet
    {
        public void Header(IXLWorksheet ws, IXLRow headerRow)
        {
            headerRow.Style.Fill.BackgroundColor = XLColor.SteelBlue;
            headerRow.Style.Font.FontColor = XLColor.White;
            headerRow.Style.Font.Bold = true;
            
            string[] columns = { "ArtistName", "Title", "Size", "Extension", "Width x Height", "HH:MM:SS", "FoundInFolder", "SizeInBytes" };
            for (var i = 0; i < columns.Length; i++)
            {
                headerRow.Cell(i+1).Value = columns[i];
            }

            var byteSizeColumn = ws.Column(6);
            byteSizeColumn.Style.NumberFormat.NumberFormatId = 3;

        }

        public IEnumerable<object> Row(List<CollectionEntry> entries)
        {
            return entries.Select(m =>
            {
                var widthHeight = "";
                var duration = "";
                var videoType = "";
                var vi = m.VideoInfo.FirstOrDefault();
                if (vi != null)
                {
                    videoType = vi.VideoType;

                    if (vi.width > 0 && vi.height > 0)
                        widthHeight = vi.width + " x " + vi.height;

                    if (vi.duration > 0)
                    {
                        var elapsed = TimeSpan.FromSeconds(vi.duration);
                        duration = $"{elapsed:h\\:mm\\:ss}";
                    }
                }
                var p = new
                {
                    Artist = m.ArtistName,
                    Title = m.EntryName,
                    Size = m.EntrySize.ToSize(2),
                    Extension = videoType,
                    WidthHeight = widthHeight,
                    Duration = duration,
                    RootFolder = m.RootFolder,
                    SizeInBytes = m.EntrySize
                };
                return p;
            });
        }
        public IEnumerable<(string, int, string)> RowComments(List<CollectionEntry> entries)
        {
            return null;
        }
        string ICollectionSpreadsheet.SortOrder()
        {
            return "ArtistName, Title, SizeInBytes ASC";
        }

    }
}
