﻿using ClosedXML.Excel;
using MusicVideoCatalog.DataTypes;
using System.Collections.Generic;
using System.Linq;

namespace MusicVideoCatalog.WorksheetProcessors
{
    public class Airchecks : ICollectionSpreadsheet
    {
        public void Header(IXLWorksheet ws, IXLRow headerRow)
        {
            headerRow.Style.Fill.BackgroundColor = XLColor.SteelBlue;
            headerRow.Style.Font.FontColor = XLColor.White;
            headerRow.Style.Font.Bold = true;            
            string[] columns = {"Title", "Size", "VideoTypes", "SizeInBytes"};
            for (var i = 0; i < columns.Length; i++)
            {
                headerRow.Cell(i + 1).Value = columns[i];
            }

            headerRow.Cell(1).Value = "Title";
            headerRow.Cell(2).Value = "Size";
            headerRow.Cell(3).Value = "VideoTypes";
            headerRow.Cell(4).Value = "SizeInBytes";

            var byteSizeColumn = ws.Column(5);
            byteSizeColumn.Style.NumberFormat.NumberFormatId = 3;
        }

        public IEnumerable<object> Row(List<CollectionEntry> entries)
        {
            return entries.Select(m => new
            {
                Title = m.EntryName,
                Size = m.EntrySize.ToSize(2),
                VideoTypes = m.VideoInfo.GetComment(),
                SizeInBytes = m.EntrySize
            });
        }

        public IEnumerable<(string, int, string)> RowComments(List<CollectionEntry> entries)
        {
            return VideoInfo.VideoRowComments(entries, "C");
        }

        public string SortOrder()
        {
            return "Title, SizeInBytes ASC";
        }
    }
}
