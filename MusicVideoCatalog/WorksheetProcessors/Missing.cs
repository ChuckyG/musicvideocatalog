﻿using ClosedXML.Excel;
using MusicVideoCatalog.DataTypes;
using System.Collections.Generic;
using System.Linq;

namespace MusicVideoCatalog.WorksheetProcessors
{
    public class Missing : ICollectionSpreadsheet
    {
        public void Header(IXLWorksheet ws, IXLRow headerRow)
        {
            headerRow.Style.Fill.BackgroundColor = XLColor.SteelBlue;
            headerRow.Style.Font.FontColor = XLColor.White;
            headerRow.Style.Font.Bold = true;
            
            string[] columns = { "Artist", "Title" };
            for (var i = 0; i < columns.Length; i++)
            {
                headerRow.Cell(i+1).Value = columns[i];
            }

            var byteSizeColumn = ws.Column(10);
            byteSizeColumn.Style.NumberFormat.NumberFormatId = 3;

        }
                
        IEnumerable<object> ICollectionSpreadsheet.Row(List<CollectionEntry> entries)
        {
            return entries.Select(m => new
            {
                Artist = m.ArtistName,
                Title = m.EntryName
            });
        }
        public IEnumerable<(string, int, string)> RowComments(List<CollectionEntry> entries)
        {
            return null;
        }
        string ICollectionSpreadsheet.SortOrder()
        {
            return "Artist, Title ASC";
        }

    }
}
