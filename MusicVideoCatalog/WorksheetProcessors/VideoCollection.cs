﻿using ClosedXML.Excel;
using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicVideoCatalog.WorksheetProcessors
{
    public class VideoCollection : ICollectionSpreadsheet
    {
        public void Header(IXLWorksheet ws, IXLRow headerRow)
        {
            headerRow.Style.Fill.BackgroundColor = XLColor.SteelBlue;
            headerRow.Style.Font.FontColor = XLColor.White;
            headerRow.Style.Font.Bold = true;

            string[] columns = { "ArtistName", "Title", "IsBootleg", "Size", "VideoTypes", "HasDVDs", "HasBluRays", "HasVideoFiles", "FoundInFolder", "SizeInBytes" };
            for (var i = 0; i < columns.Length; i++)
            {
                headerRow.Cell(i + 1).Value = columns[i];
            }

            var byteSizeColumn = ws.Column(10);
            byteSizeColumn.Style.NumberFormat.NumberFormatId = 3;
        }

        public IEnumerable<object> Row(List<CollectionEntry> entries)
        {
            return entries.Select(m => new
            {
                Artist = m.ArtistName,
                Title = m.EntryName,
                Bootleg = m.IsBootleg,
                Size = m.EntrySize.ToSize(2),
                VideoTypes = m.VideoInfo.GetComment(),
                m.HasDVDs,
                m.HasBDs,
                m.HasVideoFiles,
                m.RootFolder,
                SizeInBytes = m.EntrySize
            });
        }
        public IEnumerable<(string, int, string)> RowComments(List<CollectionEntry> entries)
        {
            return VideoInfo.VideoRowComments(entries, "E");
        }

        string ICollectionSpreadsheet.SortOrder()
        {
            return "ArtistName, Title, SizeInBytes ASC";
        }

    }
}
