﻿using Utf8Json;
using Utf8Json.Resolvers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using System;
using MusicVideoCatalog.WorksheetProcessors;
using System.Threading.Tasks;
using MusicVideoCatalog.DataTypes;

namespace MusicVideoCatalog
{
    public class CollectionList
    {
        public IEnumerable<Collection> Collections;
        public Configuration Configuration;
        private XLWorkbook _xmlWorkbook;
        public CollectionList()
        {
        }

        /// <summary>
        /// Loads the json configuration file, and also loads directory info for any drive present
        /// </summary>
        /// <param name="PathAndFilename"></param>
        /// <returns></returns>
        public static async Task<CollectionList> LoadConfigFile(string PathAndFilename)
        {
            var configFile = await File.ReadAllTextAsync(PathAndFilename);
            JsonSerializer.SetDefaultResolver(StandardResolver.Default);
            var collectionList = JsonSerializer.Deserialize<CollectionList>(configFile);
            collectionList.LoadDriveInfo();
            collectionList.Configuration.VideoExtensionsHash = new HashSet<string>(collectionList.Configuration.VideoExtensions, StringComparer.OrdinalIgnoreCase);

            collectionList.Configuration.SkipExtensionsHash = new HashSet<string>(collectionList.Configuration.SkipExtensions, StringComparer.OrdinalIgnoreCase);

            collectionList.Configuration.DVDExtensions = new List<string>() { ".DVD9", ".DVD5" };
            collectionList.Configuration.DVDExtensionsHash = new HashSet<string>(collectionList.Configuration.DVDExtensions, StringComparer.OrdinalIgnoreCase);

            collectionList.Configuration.BDExtensions = new List<string>() { ".BD50", ".BD25" };
            collectionList.Configuration.BDExtensionsHash = new HashSet<string>(collectionList.Configuration.BDExtensions, StringComparer.OrdinalIgnoreCase);
            return collectionList;
        }

        internal ICollectionSpreadsheet GetWorksheetProcess(string worksheetType)
        {
            // could do this with reflection, but I choose not to
            return worksheetType switch
            {
                "VideoTracks" => new VideoTracks(),
                "VideoCollection" => new VideoCollection(),
                "Airchecks" => new Airchecks(),
                "VariousVideoCollection" => new VariousVideoCollection(),
                "VideoSeries" => new VideoSeries(),
                "Missing" => new Missing(),
                "Movies" => new Movies(),
                "TV" => new TV(),
                _ => throw new Exception("Undefined worksheet type"),
            };
        }

        /// <summary>
        /// Generate an Excel spreadsheet with multiple worksheets
        /// </summary>
        internal void OutputCatalog()
        {
            // initialize document
            _xmlWorkbook = new XLWorkbook();

            // build all worksheets
            var worksheets = Collections.Select(o => o.CatalogSheet).Distinct();
            foreach (var worksheet in worksheets)
            {
                var worksheetCollections = Collections
                    .Where(x => x.CatalogSheet.Equals(worksheet))
                    .OrderBy(y => y.CatalogOrder);
                var entries = worksheetCollections.Select(z => z.GetEntries());
                if (entries.Count() < 1)
                    Console.WriteLine($"No entries for {worksheet}");

                CreateWorksheet(worksheet, worksheetCollections.FirstOrDefault().WorksheetType, entries);
            }

            // create single missing, should be easy to refactor if we want other types later we can add a propert to collections array in json file
            var missingCollections = Collections
                .Where(x => x.CatalogSheet.Equals("Music Videos"))
                .OrderBy(y => y.CatalogOrder)
                .Select(z => z.GetMissingEntries());
            CreateWorksheet("Missing Music Videos", "Missing", missingCollections);

            // save it
            _xmlWorkbook.SaveAs(Configuration.OutputPathAndFile + ".xlsx");
        }

        /// <summary>
        /// loop through collection entries to build a single worksheet in the spreadsheet
        /// </summary>
        /// <param name="worksheetName"></param>
        /// <param name="worksheetProcessName"></param>
        /// <param name="worksheetCollections"></param>
        internal void CreateWorksheet(string worksheetName, string worksheetProcessName, IEnumerable<List<CollectionEntry>> worksheetCollections)
        {
            var ws = _xmlWorkbook.Worksheets.Add(worksheetName);            
            var worksheetProcess = GetWorksheetProcess(worksheetProcessName);

            var header = ws.Row(1).InsertRowsAbove(1).First();
            worksheetProcess.Header(ws, header);

            var currentRow = 2;
            foreach (var worksheetCollection in worksheetCollections)
            {
                var list = worksheetCollection.ToList();
                var entries = worksheetProcess.Row(list);
                ws.Cell(currentRow, 1).InsertData(entries);

                // we could potentially fetch a column, and then whichever rows need to be set with an ienumerable
                //ws.Cell("A5").Comment.AddText("sample comment");
                var comments = worksheetProcess.RowComments(list);
                if (comments != null)
                {                    
                    foreach (var (column, row, comment) in comments)
                    {
                        var cell = ws.Cell(column + (row + currentRow).ToString());
                        cell.Comment.AddText(comment);
                        cell.Comment.Style.Size.SetAutomaticSize();
                    }
                }

                currentRow += entries.Count();

            }
            ws.Columns().AdjustToContents(1, 50); // base widths on first 50 columns
            //NEED TO ADD CELL COMMENTS for video columns where the cell > 2 rows - need summary for multiple video files

            var rangeTable = ws.RangeUsed().CreateTable();
            rangeTable.Sort(columnsToSortBy: worksheetProcess.SortOrder());
        }               


        /// <summary>
        /// look at all internal drives that are fixed or removable to see if volume name matches one we are looking for
        /// </summary>
        internal void LoadDriveInfo()
        {
            var drives = DriveInfo.GetDrives().Where(x=>x.DriveType.Equals(DriveType.Fixed) || x.DriveType.Equals(DriveType.Removable) );
            var driveInfoDictionary = drives.ToDictionary(item => item.VolumeLabel,
                                       item => item, StringComparer.OrdinalIgnoreCase);

            Collections
                .Where(x => driveInfoDictionary.ContainsKey(x.VolumeLabel))
                .ToList()
                .ForEach(y => y.DriveInfo = driveInfoDictionary[y.VolumeLabel]);
        }
    }
}
