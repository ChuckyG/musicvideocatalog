﻿using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MusicVideoCatalog.CollectionProcessors
{
    /// <summary>
    /// folder structure is folder names in folder
    /// </summary>
    public class RootFolders : CollectionProcessorBase
    {
        long entriesAdded;
        public RootFolders(Collection collection, List<CollectionEntry> entries, List<MissingEntry> missing) : base(collection, entries, missing) { }

        public override long ProcessDirectory(DirectoryInfo di)
        {
            entriesAdded = 0;
            var videoFolders = di.GetDirectories();
            foreach (var videoFolder in videoFolders)
            {
                var length = FileSystemHelpers.GetDirectorySize(videoFolder);
                var artistName = FindArtistName(videoFolder.Name);
                var videoInfo = new ConcurrentBag<VideoInfo>();
                FileSystemHelpers.GetMatchingExtensions(videoFolder, videoInfo, collection.config.VideoExtensionsHash);
                AddEntry(artistName, videoFolder.Name, length, di.FullName, videoInfo);
                entriesAdded++;
            }
            return entriesAdded;
        }
    }
}
