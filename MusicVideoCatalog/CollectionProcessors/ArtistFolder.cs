﻿using MusicVideoCatalog.DataTypes;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;

namespace MusicVideoCatalog.CollectionProcessors
{
    /// <summary>
    /// Folder structure is artist folder then foldername 
    /// </summary>
    public class ArtistFolder : CollectionProcessorBase
    {
        long entriesAdded;

        public ArtistFolder(Collection collection, List<CollectionEntry> entries, List<MissingEntry> missing) : base(collection, entries, missing) { }

        public override long ProcessDirectory(DirectoryInfo di)
        {
            entriesAdded = 0;

            foreach (var d in di.GetDirectories())
            {
                var videoFolders = d.GetDirectories();
                foreach (var videoFolder in videoFolders)
                {
                    var length = FileSystemHelpers.GetDirectorySize(videoFolder);
                    var artistName = d.Name;
                    var videoInfoBag = new ConcurrentBag<VideoInfo>();
                    FileSystemHelpers.GetMatchingExtensions(videoFolder, videoInfoBag, collection.config.VideoExtensionsHash);
                    AddEntry(artistName, videoFolder.Name, length, videoFolder.Parent.FullName, videoInfoBag);
                    entriesAdded++;
                }
            }
            return entriesAdded;
        }
    }
}
