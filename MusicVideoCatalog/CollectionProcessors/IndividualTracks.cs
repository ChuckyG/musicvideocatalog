﻿using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicVideoCatalog.CollectionProcessors
{
    /// <summary>
    /// folder structure is filenames under folder
    /// </summary>
    public class IndividualTracks : CollectionProcessorBase
    {
        long entriesAdded;
        public IndividualTracks(Collection collection, List<CollectionEntry> entries, List<MissingEntry> missing) : base(collection, entries, missing) { }

        public override long ProcessDirectory(DirectoryInfo di)
        {
            entriesAdded = 0;
            var fileBag = new ConcurrentBag<FileInfo>();
            FileSystemHelpers.GetBagOfFiles(fileBag, di);

            Parallel.ForEach(
                fileBag,
                new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                async (videoFile) => {
                    await AddBagEntries(di, videoFile);
                });
            return entriesAdded;
        }

        internal async Task AddBagEntries(DirectoryInfo di, FileInfo videoFile)
        {
            var ext = videoFile.Extension.ToUpper();
            var artistName = FindArtistName(videoFile.Name);
            if (collection.config.MissingExtension.Equals(ext))
            {
                AddMissing(videoFile.Name);
                return;
            }

            if (!collection.config.VideoExtensions.Contains(ext))
            {
                return;
            }

            var videoInfoBag = new ConcurrentBag<VideoInfo>();
            var vi = new VideoInfo()
            {
                size = videoFile.Length,
                VideoType = videoFile.Extension
            };
            await VideoAnalysis.PopulateVideoMeta(vi, videoFile);
            videoInfoBag.Add(vi);
            AddEntry(artistName, videoFile.Name, videoFile.Length, di.FullName, videoInfoBag);
            entriesAdded++;
        }

    }
}
