﻿using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MusicVideoCatalog.CollectionProcessors
{
    public abstract class CollectionProcessorBase
    {
        private readonly List<CollectionEntry> _entries;
        private readonly List<MissingEntry> _missing;
        public Collection collection;

        protected CollectionProcessorBase(Collection collection, List<CollectionEntry> entries, List<MissingEntry> missing)
        {            
            _entries = entries;
            _missing = missing;
            this.collection = collection;
        }

        public abstract long ProcessDirectory(DirectoryInfo di);

        public virtual bool HasDVDs(ConcurrentBag<VideoInfo> videoInfo)
        {
            return videoInfo.Where(x => collection.config.DVDExtensionsHash.Any(y => y.Equals(x.VideoType, StringComparison.OrdinalIgnoreCase))).Any();
        }

        public virtual bool HasBDs(ConcurrentBag<VideoInfo> videoInfo)
        {
            return videoInfo.Where(x => collection.config.BDExtensionsHash.Any(y => y.Equals(x.VideoType, StringComparison.OrdinalIgnoreCase))).Any();
        }

        public virtual bool HasVideoFiles(ConcurrentBag<VideoInfo> videoInfo)
        {
            return videoInfo.Where(x => collection.config.VideoExtensionsHash.Any(y => y.Equals(x.VideoType, StringComparison.OrdinalIgnoreCase))).Any();
        }

        public virtual void AddEntry(string artistName, string entryName, long entrySize, string folder, ConcurrentBag<VideoInfo> videoInfo)
        {
            _entries.Add(new CollectionEntry()
            {
                ArtistName = artistName,
                EntryName = entryName,
                EntrySize = entrySize,
                VideoInfo = videoInfo,
                HasDVDs = HasDVDs(videoInfo),
                HasBDs = HasBDs(videoInfo),
                HasVideoFiles = HasVideoFiles(videoInfo),
                IsBootleg = !collection.OfficialReleases, // potentially override based on folder name
                RootFolder = folder.Substring(3) // remove drive letters as they are meaningless for portable drives
            });
        }

        public virtual void AddMissing(string entry)
        {
            var artistName = FindArtistName(entry);
            var entryName = FindTitle(entry);
            _missing.Add(new MissingEntry
            {
                ArtistName = artistName,
                EntryName = entryName
            });
        }

        public string FindArtistName(string folderName)
        {
            var endOfArtistName = folderName.IndexOf(" - ");
            if (endOfArtistName < 0)
                return null;

            return folderName.Substring(0, endOfArtistName);
        }

        public string FindTitle(string folderName)
        {
            var separator = " - ";
            var extensionLength = Path.GetExtension(folderName).Length;
            var artistNameEndingPosition = folderName.IndexOf(separator);
            if (artistNameEndingPosition < 0)
                return null;

            return folderName.Substring(artistNameEndingPosition + separator.Length, 
                folderName.Length - artistNameEndingPosition - separator.Length - extensionLength);
        }
    }
}
