﻿using Alturos.VideoInfo;
using System;
using System.Threading.Tasks;

namespace MusicVideoCatalog
{
    /// <summary>
    /// Build a catalog file of current drives, and then create a catalog file from files built.
    /// TODO: add TV, animation and movies drives. perhaps with different .json files for different types
    ///         cache video info lookup, (check if filesize is changed, then refresh video info) 
    ///         investigate why videoinfo times/dimensions are sometimes wildly off
    ///         figure out how to initialize VideoAnaylsis object once and share among threads? not sure how to make threadsafe yet
    ///         build list of potential problem files/folders
    /// </summary>
    class Program
    {
        public static void Main(string[] args)
        {          
            var collectionList = CollectionList.LoadConfigFile("Collections.json").Result;
            if (FileSystemHelpers.IsFileLocked(collectionList.Configuration.OutputPathAndFile + ".xlsx"))
            {
                Console.WriteLine("Please close the Excel file and run again.");
                Environment.Exit(-1);
                return;
            }
            
            foreach (var collection in collectionList.Collections)
            {
                collection.ProcessCollection(collectionList.Configuration);
            }

            collectionList.OutputCatalog();            
        }        
    }
}
