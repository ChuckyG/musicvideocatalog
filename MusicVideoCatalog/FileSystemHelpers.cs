﻿using Alturos.VideoInfo;
using DocumentFormat.OpenXml.Spreadsheet;
using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MusicVideoCatalog
{
    public static class FileSystemHelpers
    {
        // stackoverflow to the rescue
        // copied in from personal FileAndFolderLibrary
        public static long GetDirectorySize(DirectoryInfo directoryInfo, bool recursive = true)
        {
            var startDirectorySize = default(long);
            if (directoryInfo == null || !directoryInfo.Exists)
                return startDirectorySize; //Return 0 while Directory does not exist.

            //Add size of files in the Current Directory to main size.
            foreach (var fileInfo in directoryInfo.GetFiles())
                System.Threading.Interlocked.Add(ref startDirectorySize, fileInfo.Length);

            if (recursive) //Loop on Sub Direcotries in the Current Directory and Calculate it's files size.
                System.Threading.Tasks.Parallel
                    .ForEach(directoryInfo.GetDirectories(), 
                        (subDirectory) => System.Threading.Interlocked
                            .Add(ref startDirectorySize, GetDirectorySize(subDirectory, recursive)));

            return startDirectorySize;  //Return full Size of this Directory.
        }

        /// <summary>
        /// grab all the files contained with folder and subfolder of directoryInfo passed in
        /// </summary>
        /// <param name="fileBag"></param>
        /// <param name="directoryInfo"></param>
        /// <param name="recursive"></param>
        public static void GetBagOfFiles(ConcurrentBag<FileInfo> fileBag, DirectoryInfo directoryInfo, bool recursive = true)
        {            
            if (directoryInfo == null || !directoryInfo.Exists)
                return;

            //Add size of files in the Current Directory to main size.
            Parallel.ForEach(directoryInfo.GetFiles(),
                new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                (fileInfo) => fileBag.Add(fileInfo));

            if (recursive) //Loop on Sub Direcotries in the Current Directory and those
                Parallel.ForEach(
                    directoryInfo.GetDirectories(), 
                    new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, 
                    (subDirectory) => GetBagOfFiles(fileBag, subDirectory, recursive));
        }

        /// <summary>
        /// what size bluray media
        /// </summary>
        /// <param name="entrySize"></param>
        /// <returns></returns>
        internal static string PickBDBasedOnSize(long entrySize)
        {
            var BD50MinSize = 26843531857;
            if (entrySize > BD50MinSize)
                return ".BD50";
            else
                return ".BD25";
        }

        /// <summary>
        /// use with folders named VIDEO_TS or ISO files
        /// </summary>
        /// <param name="entrySize"></param>
        /// <returns></returns>
        internal static string PickDVDBasedOnSize(long entrySize)
        {
            var DVD9MinSize = 4831835735;
            if (entrySize > DVD9MinSize)
                return ".DVD9";
            else 
                return ".DVD5";
        }

        /// <summary>
        /// based on file size only, determine if bluray or DVD iso
        /// will not detect a bluray that is smaller than a DVD9, but that's fairly rare
        /// </summary>
        /// <param name="entrySize"></param>
        /// <returns></returns>
        internal static string PickISOBasedOnSize(long entrySize)
        {
            var DVD9MAXSize = 9663671469;

            // too big to be DVD
            if (entrySize > DVD9MAXSize)
                return PickBDBasedOnSize(entrySize);
            else
                return PickDVDBasedOnSize(entrySize);
        }

        internal static async void AddToBag(FileInfo fileInfo, ConcurrentBag<VideoInfo> videoInfoBag, HashSet<string> extensions)
        {
            var vi = new VideoInfo()
            {
                size = fileInfo.Length,
                VideoType = fileInfo.Extension
            };

            // DVDs and BLURAY ISO files are handled differently
            if (fileInfo.Extension.Equals(".ISO", StringComparison.InvariantCultureIgnoreCase))
            {
                vi.VideoType = PickISOBasedOnSize(fileInfo.Length);
                videoInfoBag.Add(vi);
                return;
            }

            var ext = fileInfo.Extension.ToUpper();
            if (extensions.Contains(ext))
            {
                await VideoAnalysis.PopulateVideoMeta(vi, fileInfo);
                videoInfoBag.Add(vi);
            }
        }

        /// <summary>
        /// if we find VIDEO_TS folder, it is a DVD  
        /// if we find an ISO, determine if it is a DVD5, DVD9 or BD25, BD50 based on size 
        /// if we find stream folder it is bluray
        /// 
        /// return a list of all extensions found, we can then count the extensions later
        /// </summary>
        /// <param name="directoryInfo"></param>
        /// <param name="extensions"></param>
        /// <returns></returns>
        public static void GetMatchingExtensions(DirectoryInfo directoryInfo, ConcurrentBag<VideoInfo> videoInfoBag, HashSet<string> extensions)
        {
            if (directoryInfo == null || !directoryInfo.Exists)
                return; //Return null while Directory does not exist
            
            // if we have a DVD VIDEO_TS we only need to know the size, we can stop recursion here
            if (directoryInfo.Name.Equals("VIDEO_TS", StringComparison.InvariantCultureIgnoreCase))
            {
                var dirSize = GetDirectorySize(directoryInfo);
                videoInfoBag.Add(new VideoInfo()
                {
                    VideoType = PickDVDBasedOnSize(dirSize),
                    size = dirSize
                });                
                return;
            }
            // skip audio only folders in DVDs
            if (directoryInfo.Name.Equals("AUDIO_TS", StringComparison.InvariantCultureIgnoreCase) || directoryInfo.Name.Equals("EXTRAS_TS", StringComparison.InvariantCultureIgnoreCase))
                return;

            Parallel.ForEach(directoryInfo.GetFiles(),
                new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                (fileInfo) => AddToBag(fileInfo, videoInfoBag, extensions));

            var directories = directoryInfo.GetDirectories();

            // if we have a bluray folder, we only need to know the size of the current folder & subfolders, we can stop recursion here
            if (directories.Any(x => x.Name.Equals("BDMV", StringComparison.InvariantCultureIgnoreCase)))
            {
                var dirSize = GetDirectorySize(directoryInfo);
                videoInfoBag.Add(new VideoInfo() {
                    size = dirSize,
                    VideoType = PickBDBasedOnSize(dirSize)
                });
                return;
            }

            //Loop on Sub Directories in the Current Directory for more folders/file extensions
            Parallel.ForEach(
                directories,
                new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                (subDirectory) => GetMatchingExtensions(subDirectory, videoInfoBag, extensions));
        }

        // copied from stack overflow
        public static bool IsFileLocked(string filename)
        {
            try
            {
                var Info = new FileInfo(filename);
                using (var stream = Info.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
    }

    public static class MyExtension
    {
        public enum SizeUnits
        {
            Byte, KB, MB, GB, TB, PB, EB, ZB, YB
        }

        public static string ToSize(this Int64 value, SizeUnits unit)
        {
            return (value / (double)Math.Pow(1024, (Int64)unit)).ToString("0.00");
        }

        public static string[] SizeSuffixes =
                   { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        public static string ToSize(this Int64 value, int decimalPlaces=1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + ToSize(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

    }
}
