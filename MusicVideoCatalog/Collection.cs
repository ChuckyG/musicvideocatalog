﻿using MusicVideoCatalog.CollectionProcessors;
using MusicVideoCatalog.DataTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Utf8Json;
using Utf8Json.Resolvers;

namespace MusicVideoCatalog
{
    public class Collection
    {
        public string VolumeLabel;
        public CollectionType CollectionType;
        public string[] Folders;
        public bool OfficialReleases;
        public bool IndividualTracks;
        public string CatalogFile;
        public int CatalogOrder;
        public string CatalogSheet;
        public List<CollectionEntry> entries;
        public List<MissingEntry> missing;
        public string WorksheetType;

        [IgnoreDataMember]
        public DriveInfo DriveInfo;
        [IgnoreDataMember]
        public Configuration config;

        /// <summary>
        /// if catalog drive is not active, load from an existing file
        /// otherwise loop through all folders to build json file for the collection
        /// </summary>
        public void ProcessCollection(Configuration config)
        {
            this.config = config;
            missing = new List<MissingEntry>();
            entries = new List<CollectionEntry>();

            if (DriveInfo == null)
            {
                var collectionFile = LoadCollectionFile(CatalogFile + ".json");
                if (collectionFile == null)
                {
                    Console.WriteLine($"{CatalogFile} catalog file not present, drive not connected either");
                    return;
                }

                entries = collectionFile.entries;
                missing = collectionFile.missing;
                return; 
            }

            foreach (var folder in Folders)
            {
                var di = new DirectoryInfo(Path.Combine(DriveInfo.Name, folder));
                CollectionProcessorBase processor = CollectionType switch
                {
                    CollectionType.ArtistFolders => new ArtistFolder(this, entries, missing),
                    CollectionType.IndividualTracks => new IndividualTracks(this, entries, missing),
                    CollectionType.RootFolders => new RootFolders(this, entries, missing),
                    CollectionType.Unknown => throw new NotImplementedException(),
                    CollectionType.Undefined => throw new NotImplementedException(),
                    _ => throw new NotImplementedException()
                };
                var addedEntries = processor.ProcessDirectory(di);
                if (addedEntries < 1)
                    Console.WriteLine($"Found no entries for {folder}");
            }
            JsonSerializer.SetDefaultResolver(StandardResolver.Default);
            var result = JsonSerializer.Serialize(this);
            result = JsonSerializer.PrettyPrintByteArray(result);
            File.WriteAllBytes(CatalogFile + ".json", result);
        }

        public static Collection LoadCollectionFile(string pathAndFilename)
        {
            if (!File.Exists(pathAndFilename))
                return null;

            var catalogFile = File.ReadAllBytes(pathAndFilename);
            JsonSerializer.SetDefaultResolver(StandardResolver.Default);
            return JsonSerializer.Deserialize<Collection>(catalogFile);
        }

        public List<CollectionEntry> GetMissingEntries()
        {
            if (missing == null)
                return null;

            return missing
                  .Select(x => new CollectionEntry() { ArtistName = x.ArtistName, EntryName = x.EntryName })
                  .ToList();
        }

        public List<CollectionEntry> GetEntries()
        {            
            return entries;
        }

    }
}
