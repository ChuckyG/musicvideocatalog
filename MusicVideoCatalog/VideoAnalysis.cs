﻿using Alturos.VideoInfo;
using MusicVideoCatalog.DataTypes;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MusicVideoCatalog
{
    public class VideoAnalysis
    {
        public static async Task<VideoAnalyzer> LoadVideoAnalyzer()
        {
            var fileDownloader = new FileDownloader();
            var result = await fileDownloader.DownloadAsync("ffmpeg");

            if (!result.Successful)
            {
                throw new Exception("Could not load ffmeg probe");
            }
            var ffprobePath = result.FfprobePath;
            return new VideoAnalyzer(ffprobePath);
        }

        public async static Task PopulateVideoMeta(VideoInfo vi, FileInfo fileInfo)
        {
            var videoAnalyzer = await LoadVideoAnalyzer();
            var analyzeResult = await videoAnalyzer.GetVideoInfoAsync(fileInfo.FullName);
            // if file size is too big, it won't pull info due to int/long conversion in newtonsoft json
            if (!analyzeResult.Successful)
            {
                return;
            }

            var videoStream = analyzeResult.VideoInfo.Streams.FirstOrDefault(o => o.CodecType == "video");
            if (videoStream == null) // skip audio only files for now, and yes people use video formats without video stream for audio sometimes
                return;

            vi.height = videoStream.Height;
            vi.width = videoStream.Width;
            vi.duration = videoStream.Duration;

        }
    }
}
