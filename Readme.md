﻿# Music Video Catalog
Builds a json file for each drive in root config file.  Will then create an Excel document

### How to use it
Edit Collection.json to specify volume label of drives, and what type of folder structure it uses
 * Folder structure is artist folder then foldername 
 * folder structure is filenames under folder
 * folder structure is folder names in folder

#### Unit Testing
Nothing yet
 
#### Future Enhancements
Maybe read in an info document from each folder for additional details.